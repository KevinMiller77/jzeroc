workspace "Jzero"
    architecture "arm64"
    startproject "JzeroC"
    configurations 
    { 
        "Debug",
        "Release"
    }

    flags
    {
        "MultiProcessorCompile"
    }

    outputdir = "%{cfg.buildcfg}/%{cfg.system}%{cfg.architecture}"
    
    group "Dependencies"
        -- include "Nebula/ext/glad"
        group ""
        
project "JzeroC"
    kind "ConsoleApp"
    language "C++"
    cppdialect "C++17"
    staticruntime "on"  
    
    
    targetname("jzeroc")
    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-int/" .. outputdir .. "/%{prj.name}")
    location ("build")

    debugdir("./JzeroC")

    prebuildcommands {
        "../scripts/performYacc.sh",
        "../scripts/performFlex.sh"
    }

    files
    {
        "%{prj.name}/src/**.cpp"
    }

    defines
    {
        -- "SCANNER_DEBUG"
    }

    externalincludedirs { 
        "%{prj.name}/include",
        "%{prj.name}/include/flex",
        "%{prj.name}/include/yacc",
    }

    -- links { }

    filter "system:windows"
        ignoredefaultlibraries
        {
            "user32", 
            "gdi32", 
            "shell32",
            "msvcrtd"
        }

    filter "system:macosx"
        buildoptions {
            "-Wno-macro-redefined",
            "-Wno-pragma-once-outside-header",
            "-stdlib=libc++",
            "-F /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk/System/Library/Frameworks/",
        }


    filter "system:linux"

    filter "configurations:Debug"
        defines "NEB_DEBUG"
        runtime "Debug"
        symbols "on"

    filter "configurations:Release"
        defines "NEB_RELEASE"
        runtime "Release"
        optimize "on"
