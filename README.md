### JZero

As designed in the book ["Build Your Own Programming Language"](https://www.google.com/books/edition/Build_Your_Own_Programming_Language/7vFQEAAAQBAJ?hl=en&gbpv=0)

### Dependencies

* [Flex](https://github.com/westes/flex) libfl - Fast lexical analyzer 