/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

#ifndef YY_YY_JZEROC_SRC_YACC_YYPARSE_HPP_INCLUDED
# define YY_YY_JZEROC_SRC_YACC_YYPARSE_HPP_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif
/* "%code requires" blocks.  */
#line 11 "../JzeroC/src/yacc/javagrammar.ypp"

   #include "../jzeroc/jzeroc.h"

#line 53 "../JzeroC/src/yacc/yyparse.hpp"

/* Token kinds.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    YYEMPTY = -2,
    YYEOF = 0,                     /* "end of file"  */
    YYerror = 256,                 /* error  */
    YYUNDEF = 257,                 /* "invalid token"  */
    BREAK = 258,                   /* BREAK  */
    DOUBLE = 259,                  /* DOUBLE  */
    ELSE = 260,                    /* ELSE  */
    FOR = 261,                     /* FOR  */
    IF = 262,                      /* IF  */
    INT = 263,                     /* INT  */
    RETURN = 264,                  /* RETURN  */
    VOID = 265,                    /* VOID  */
    WHILE = 266,                   /* WHILE  */
    IDENTIFIER = 267,              /* IDENTIFIER  */
    CLASSNAME = 268,               /* CLASSNAME  */
    CLASS = 269,                   /* CLASS  */
    STRING = 270,                  /* STRING  */
    BOOL = 271,                    /* BOOL  */
    INTLIT = 272,                  /* INTLIT  */
    DOUBLELIT = 273,               /* DOUBLELIT  */
    STRINGLIT = 274,               /* STRINGLIT  */
    BOOLLIT = 275,                 /* BOOLLIT  */
    NULLVAL = 276,                 /* NULLVAL  */
    LESSTHANOREQUAL = 277,         /* LESSTHANOREQUAL  */
    GREATERTHANOREQUAL = 278,      /* GREATERTHANOREQUAL  */
    ISEQUALTO = 279,               /* ISEQUALTO  */
    NOTEQUALTO = 280,              /* NOTEQUALTO  */
    LOGICALAND = 281,              /* LOGICALAND  */
    LOGICALOR = 282,               /* LOGICALOR  */
    INCREMENT = 283,               /* INCREMENT  */
    DECREMENT = 284,               /* DECREMENT  */
    PUBLIC = 285,                  /* PUBLIC  */
    STATIC = 286,                  /* STATIC  */
    YYERRCODE = 287                /* YYERRCODE  */
  };
  typedef enum yytokentype yytoken_kind_t;
#endif
/* Token kinds.  */
#define YYEMPTY -2
#define YYEOF 0
#define YYerror 256
#define YYUNDEF 257
#define BREAK 258
#define DOUBLE 259
#define ELSE 260
#define FOR 261
#define IF 262
#define INT 263
#define RETURN 264
#define VOID 265
#define WHILE 266
#define IDENTIFIER 267
#define CLASSNAME 268
#define CLASS 269
#define STRING 270
#define BOOL 271
#define INTLIT 272
#define DOUBLELIT 273
#define STRINGLIT 274
#define BOOLLIT 275
#define NULLVAL 276
#define LESSTHANOREQUAL 277
#define GREATERTHANOREQUAL 278
#define ISEQUALTO 279
#define NOTEQUALTO 280
#define LOGICALAND 281
#define LOGICALOR 282
#define INCREMENT 283
#define DECREMENT 284
#define PUBLIC 285
#define STATIC 286
#define YYERRCODE 287

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef  SyntaxNode*  YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;


int yyparse (void);


#endif /* !YY_YY_JZEROC_SRC_YACC_YYPARSE_HPP_INCLUDED  */
