#pragma once

#include <string>
#include "token.h"
#include "symbol_table.h"

enum SYMBOL_RULES {
    CLASS_DECL = 1000,
    CLASS_BODY = 1010,
    CLASS_BODY_DECLS = 1020,
    FIELD_DECL = 1030,
    QUALIFIED_NAME = 1040,
    VAR_DECLS = 1050,
    VAR_DECL = 1060,
    METHOD_DECL = 1380,
    METHOD_HEADER = 1070,
    CONSTRUCTOR_DECL = 1110,
    METHOD_DECLARATOR = 1080,
    FORMAL_PARAM_LIST = 1090,
    FORMAL_PARAM = 1100,
    BLOCK = 1120,
    BLOCK_STMTS = 1130,
    LOCAL_VAR_DECL = 1140,
    IF_THEN_STMT = 1150,
    IF_THEN_ELSE_STMT = 1160,
    IF_THEN_ELSE_IF_STMT = 1170,
    ELSE_IF_SEQ = 1180,
    ELSE_IF_STMT = 1190,
    WHILE_STMT = 1210,
    FOR_STMT = 1220,
    STMT_EXPR_LIST = 1230,
    RETURN_STMT = 1250,
    ARG_LIST = 1270,
    FIELD_ACCESS = 1280,
    METHOD_CALL = 1290,
    UNARY_EXPR = 1300,
    MUL_EXPR = 1310,
    ADD_EXPR = 1320,
    REL_EXPR = 1330,
    EQ_EXPR = 1340,
    COND_AND_EXPR = 1350,
    COND_OR_EXPR = 1360,
    ASSIGNMENT = 1370,
    QUALIFIER_OPT = 1390,
};

class SyntaxNode {
public:
    SyntaxNode() = default;
    SyntaxNode(
        std::string s, 
        int r, 
        Token t
    );

    SyntaxNode(
        std::string symbol, 
        int rule, 
        std::vector<SyntaxNode*> table = std::vector<SyntaxNode*>()
    );

    // Get global unique ID
    static int getId();

    // Some rules are the same type but different values
    // This function will return the base rule which will match
    // the corresponding rule in SYMBOL_RULES
    int getBaseRule();
    int getRuleOffset();
    bool isConst();

    // Create symbol tables for this node
    void createSymbolTables(SymbolTable*);
    
    // Populate symbol tables
    void populateSymbolTables();
    void insertVarDelcs(std::string, SyntaxNode*, bool = false);
    
    // Validate symbol tables
    void validateSymbolTables();
    void validateBlocks();

    // Detect undeclared variables, returns the found symbol if it exists
    Symbol* detectUndeclaredVars();

    // Print the tree from this node
    void print(int idn = 0);
    void printSymbols(int idn = 0, bool force = false);

    void printUndefinedSymbolError();

private:
    int m_Id;
    int m_Rule;
    
    std::string m_Symbol;
    Token m_Token = Token();
    
    std::vector<SyntaxNode*> m_Children = std::vector<SyntaxNode*>();
    
    bool m_Const;
    SymbolTable* m_SymbolTable = nullptr;

    friend class SymbolTable;
};