#pragma once

#include <string>
#include <unordered_map>

class SymbolTable;
class SyntaxNode;

class Symbol {
public:
    // Non-Leaf Symbol
    Symbol(
        std::string symbol, 
        SyntaxNode* node,
        SymbolTable* parentTable,
        SymbolTable* table,
        bool isConst = true
    );

    // Leaf symbol
    Symbol(
        std::string symbol,  
        SyntaxNode* node,
        SymbolTable* parentTable,
        bool isConst = true
    );

    std::string m_Symbol;
    SyntaxNode* m_Node;
    SymbolTable* m_ParentTable;
    SymbolTable* m_Table;
    bool m_Const;
};

typedef std::unordered_map<std::string, Symbol*> SymbolMap;

class SymbolTable {
public:
    SymbolTable() = default;
    SymbolTable(std::string scope, SymbolTable* parent = nullptr);
    
    Symbol* lookup(std::string symbol);
    void insert(std::string symbol, SyntaxNode*, bool isConst, SymbolTable* table = nullptr);

    void print(int idn);
    std::string getQualifiedParent();

private:
    std::string m_Scope;
    SymbolTable* m_Parent;
    SymbolMap m_Table;

    friend class SyntaxNode;
};