#pragma once

#include <string>
#include <vector>

#include "token.h"
#include "syntax_node.h"


class yyFlexLexer;

class JzeroC {
public:
    // Create singleton instance
    static void init(const char* in, const char* out = nullptr);
    static JzeroC*     getInst() { return s_Scanner; }

    bool        isReady() { return m_Flex; }
    void        run();
    
    // Handlers
    void        logError(const char*, const char* = nullptr);
    void        lexError(const char*);
    void        parseError(const char*);

    // Parser operations
    int             doLex();
    SyntaxNode*     scan(int);
    SyntaxNode*     insertSemicolonNewline();
    bool            newline();
    void            whitespace();
    void            comment();
    void            semantic(SyntaxNode*);
    
    // Helpers
    std::string yyText();

private:
    JzeroC(const char* in, const char* out);

    int m_Line;
    int m_Col;
    Token m_Token;
    Token m_TokenPrev;

    yyFlexLexer* m_Flex = nullptr;

    static JzeroC* s_Scanner;
};