#pragma once

#include <string>

class Token {
public:
    Token() = default;
    Token(int, std::string, int, int);

    std::string procEsc(std::string);
    bool isEmpty() { return m_Category == -1; }

    int m_Category = -1;
    std::string m_Text;
    int m_Line;
    int m_Col;

    // Literals
    int m_IntLit;
    double m_DoubLit;
    std::string m_StrLit;
};