%option c++ 
%option noyywrap

%{
    #include <jzeroc/jzeroc.h>
    #include <yacc/yyparse.hpp>
    #define JZEROC JzeroC::getInst()
%}

%%
"/*"([^*]|"*"+[^/*])*"*"+"/" { JZEROC->comment(); }
"//".*\r?\n                  { JZEROC->comment(); }
[ \t\r\f]+                   { JZEROC->whitespace(); }
\n                           { if (JZEROC->newline()) { yylval = JZEROC->insertSemicolonNewline(); return ';'; } }
"break"                { yylval = JZEROC->scan(BREAK); return BREAK;  }
"double"               { yylval = JZEROC->scan(DOUBLE); return DOUBLE;  } 
"else"                 { yylval = JZEROC->scan(ELSE); return ELSE;  }
"false"                { yylval = JZEROC->scan(BOOLLIT); return BOOLLIT;  }
"for"                  { yylval = JZEROC->scan(FOR); return FOR;  }
"if"                   { yylval = JZEROC->scan(IF); return IF;  }
"int"                  { yylval = JZEROC->scan(INT); return INT;  }
"null"                 { yylval = JZEROC->scan(NULLVAL); return NULLVAL;  }
"public"               { yylval = JZEROC->scan(PUBLIC); return PUBLIC;  }
"return"               { yylval = JZEROC->scan(RETURN); return RETURN;  }
"static"               { yylval = JZEROC->scan(STATIC); return STATIC;  }
"string"               { yylval = JZEROC->scan(STRING); return STRING;  }
"true"                 { yylval = JZEROC->scan(BOOLLIT); return BOOLLIT;  }
"bool"                 { yylval = JZEROC->scan(BOOL); return BOOL;  }
"void"                 { yylval = JZEROC->scan(VOID); return VOID;  }
"while"                { yylval = JZEROC->scan(WHILE); return WHILE;  }
"class"                { yylval = JZEROC->scan(CLASS); return CLASS;  }
"("                    { yylval = JZEROC->scan('('); return '('; }
")"                    { yylval = JZEROC->scan(')'); return ')'; }
"["                    { yylval = JZEROC->scan('['); return '['; }
"]"                    { yylval = JZEROC->scan(']'); return ']'; }
"{"                    { yylval = JZEROC->scan('{'); return '{'; }
"}"                    { yylval = JZEROC->scan('}'); return '}'; }
";"                    { yylval = JZEROC->scan(';'); return ';'; }
":"                    { yylval = JZEROC->scan(':'); return ':'; }
"!"                    { yylval = JZEROC->scan('!'); return '!'; }
"*"                    { yylval = JZEROC->scan('*'); return '*'; }
"/"                    { yylval = JZEROC->scan('/'); return '/'; }
"%"                    { yylval = JZEROC->scan('%'); return '%'; }
"+"                    { yylval = JZEROC->scan('+'); return '+'; }
"-"                    { yylval = JZEROC->scan('-'); return '-'; }
"<"                    { yylval = JZEROC->scan('<'); return '<'; }
"<="                   { yylval = JZEROC->scan(LESSTHANOREQUAL); return LESSTHANOREQUAL; }
">"                    { yylval = JZEROC->scan('>'); return '>'; }
">="                   { yylval = JZEROC->scan(GREATERTHANOREQUAL); return GREATERTHANOREQUAL; }
"=="                   { yylval = JZEROC->scan(ISEQUALTO); return ISEQUALTO; }
"!="                   { yylval = JZEROC->scan(NOTEQUALTO); return NOTEQUALTO; }
"&&"                   { yylval = JZEROC->scan(LOGICALAND); return LOGICALAND; }
"||"                   { yylval = JZEROC->scan(LOGICALOR); return LOGICALOR; }
"="                    { yylval = JZEROC->scan('='); return '=';  }
"+="                   { yylval = JZEROC->scan(INCREMENT); return INCREMENT;  }
"-="                   { yylval = JZEROC->scan(DECREMENT); return DECREMENT;  }
","                    { yylval = JZEROC->scan(','); return ',';  }
"."                    { yylval = JZEROC->scan('.'); return '.';  }
[a-zA-Z_][a-zA-Z0-9_]*              { yylval = JZEROC->scan(IDENTIFIER); return IDENTIFIER;  }
[0-9]+                              { yylval = JZEROC->scan(INTLIT); return INTLIT;  }
[0-9]*"."[0-9]*([eE][+-]?[0-9]+)?   { yylval = JZEROC->scan(DOUBLELIT); return DOUBLELIT;  }
([0-9]+)([eE][+-]?([0-9]+))         { yylval = JZEROC->scan(DOUBLELIT); return DOUBLELIT;  }
\"[^\"]*\"                          { yylval = JZEROC->scan(STRINGLIT); return STRINGLIT;  }
.              { JZEROC->lexError("unrecognized character"); }
%%
