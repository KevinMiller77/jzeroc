#include <jzeroc/jzeroc.h>
#include <jzeroc/symbol_table.h>

#include <iostream>
#include <fstream>

#include <flex/FlexLexer.h>
#include <yacc/yyparse.hpp>

// Define statics
JzeroC* JzeroC::s_Scanner = nullptr;

/*
    JzeroC Method Definitions
*/
void JzeroC::init(const char* in, const char* out) {
    s_Scanner = new JzeroC(in, out);
}

JzeroC::JzeroC(const char* in, const char* out) {
    m_Line = 1;
    m_Col = 1;

    // Attempt to open input file
    std::ifstream* yyin = new std::ifstream(); 
    yyin->open(in);

    if (yyin->fail()) {
        printf("Could not open input file!\n");
        exit(1);
    }

    // If not output file given, begin without
    if (!out) {
        m_Flex = new yyFlexLexer(yyin);
        return;
    }

    // Output file given, attempt to open
    std::ofstream* yyout = new std::ofstream();
    yyout->open(out);

    if (yyout->fail()) {
        printf("Could not open output file!\n");
        exit(1);
    }

    m_Flex = new yyFlexLexer(yyin, yyout);
}

void JzeroC::run() {
#ifdef SCANNER_ONLY_MODE
    while(m_Flex->yylex()) {
        std::cout << "[" << m_Token.cat << "]\t(" << m_Token.line << ", " << m_Token.col << ")";
        std::cout << "\t: " << m_Token.text << std::endl;
    }
#endif
}

int JzeroC::doLex() {
    if (!m_Flex) {
        std::cerr << "[SCANNER] Attempted to lex without a valid lexer!" << std::endl;
    }

    return m_Flex->yylex();
}

std::string JzeroC::yyText() {
    return std::string(m_Flex->YYText());
}

void JzeroC::logError(const char* err, const char* tag) {
    const char* err_tag = tag ? tag : "MISC";
    std::cerr << "[" << err_tag << "] " << err << std::endl;
}

void JzeroC::lexError(const char* err) {
    std::cerr << "[LEXER] " << err << ": (" << m_Line << ", " << m_Col << "): " << yyText() << std::endl;
}

void JzeroC::parseError(const char* err) {
    std::cerr << "[PARSER] " << err << ": (" << m_TokenPrev.m_Line << ", " << m_TokenPrev.m_Col << "): " << m_TokenPrev.m_Text << std::endl;
}

void JzeroC::whitespace() {
    m_Col += yyText().size();
}

SyntaxNode* JzeroC::insertSemicolonNewline() {
    m_TokenPrev = m_Token;
    m_Token = Token(';', std::string(";"), m_Line, m_Col);
    
    m_Line++;
    m_Col = 1;

#ifdef SCANNER_DEBUG
    std::cout << "[" << m_Token.cat << "]\t(" << m_Token.line << ", " << m_Token.col << ")";
    std::cout << "\t: " << m_Token.text << std::endl;
#endif

    return new SyntaxNode(m_Token.m_Text, m_Token.m_Category, m_Token);
}

SyntaxNode* JzeroC::scan(int cat) {
    // Process token
    m_TokenPrev = m_Token;
    m_Token = Token(cat, yyText(), m_Line, m_Col);
    m_Col += yyText().size();

#ifdef SCANNER_DEBUG
    std::cout << "[" << m_Token.cat << "]\t(" << m_Token.line << ", " << m_Token.col << ")";
    std::cout << "\t: " << m_Token.text << std::endl;
#endif

    return new SyntaxNode(m_Token.m_Text, m_Token.m_Category, m_Token);
}

bool JzeroC::newline() {

    // Insert a semicolon if previous token is one of these
    switch (m_Token.m_Category) {
        case (IDENTIFIER):
        case (INTLIT):
        case (DOUBLELIT):
        case (STRINGLIT):
        case (BREAK):
        case (RETURN):
        case (INCREMENT):
        case (DECREMENT):
        case (')'):
        case (']'):
        case ('}'): {
            return true;
        }
    }

    // No semicolor insert, we may progress to newline
    m_Line++;
    m_Col = 1;

    return false;
}

void JzeroC::comment() {
    for (char c: yyText()) {
        if (c != '\n') {
            m_Col++;
            continue;
        }

        m_Line++;
        m_Col = 1;
    }
}

void JzeroC::semantic(SyntaxNode* root) {
    SymbolTable* global = new SymbolTable("global");
    SymbolTable* system = new SymbolTable("System");
    SymbolTable* out = new SymbolTable("out");
    out->insert("println", root, false);
    system->insert("out", root, false, out);
    global->insert("System", root, false, system);
    root->createSymbolTables(global);
    root->populateSymbolTables();
    root->validateSymbolTables();
    root->printSymbols();
}
