#include <jzeroc/symbol_table.h>
#include <jzeroc/jzeroc.h>

#include <iostream>

Symbol::Symbol(
    std::string symbol, 
    SyntaxNode* node,
    SymbolTable* parentTable, 
    SymbolTable* table, 
    bool isConst
): Symbol(symbol, node, parentTable, isConst) {
    m_Table = table;
}

Symbol::Symbol(
    std::string symbol, 
    SyntaxNode* node, 
    SymbolTable* parentTable, 
    bool isConst
) {
    m_Symbol = symbol;
    m_ParentTable = parentTable;
    m_Const = isConst;
    m_Node = node;
}

SymbolTable::SymbolTable(std::string scope, SymbolTable* parent) {
    m_Scope = scope;
    m_Parent = parent;

    m_Table = SymbolMap();
}

Symbol* SymbolTable::lookup(std::string symbol) {
    if (m_Table.find(symbol) == m_Table.end()) {
        return m_Parent ? m_Parent->lookup(symbol) : nullptr;
    }

    return m_Table[symbol];
}

void SymbolTable::insert(std::string symbol, SyntaxNode* node, bool isConst, SymbolTable* table) {
    if (m_Table.find(symbol) != m_Table.end()) {
        JzeroC::getInst()->logError("Symbol already exists in table", "SymbolTable::insert");
        return;
    }
    
    if (table) {
        table->m_Parent = this;
    }

    m_Table[symbol] = new Symbol(symbol, node, this, table, isConst);
}

std::string SymbolTable::getQualifiedParent() {
    std::string parent = m_Scope;

    SymbolTable* curr = m_Parent;
    while(curr && curr->m_Parent) {
        parent = curr->m_Scope + "." + parent;
        curr = curr->m_Parent;
    }

    return parent;
}

void SymbolTable::print(int idn) {
    if (m_Table.empty()) {
        return;
    }

    for (auto& pair: m_Table) {
        bool shouldPrint = false;
        if (!pair.second) {
            shouldPrint = true;
        }

        if (pair.second) {
            switch (pair.second->m_Node->getBaseRule()) {
                case(LOCAL_VAR_DECL):
                case(FORMAL_PARAM):
                case(METHOD_DECL):
                case(FIELD_DECL):
                case(CLASS_DECL): {
                    std::string symbol = pair.second->m_Symbol;
                    if (pair.second->m_Table) {
                        symbol = pair.second->m_Table->m_Scope;
                    }
                    std::cout << pair.second->m_Node->getBaseRule() << " " << getQualifiedParent() << "." << symbol << std::endl;
                }
            }

            if (pair.second->m_Table) {
                pair.second->m_Table->print(idn + 1);
            }
        }
    }
}