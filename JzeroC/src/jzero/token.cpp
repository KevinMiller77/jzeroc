#include <jzeroc/jzeroc.h>
#include <jzeroc/token.h>

#include <yacc/yyparse.hpp>

Token::Token(int cat, std::string txt, int line, int col) {
    this->m_Category = cat; 
    this->m_Text = txt;
    this->m_Line = line; 
    this->m_Col = col;

    switch (cat) {
        case (INTLIT): {
            m_IntLit = std::stoi(txt);
            break;
        }
        case (DOUBLELIT): {
            m_DoubLit = std::stod(txt);
            break;
        }
        case (STRINGLIT): {
            m_StrLit = txt;
            break;
        }
        case (BOOLLIT): {
            m_IntLit = txt == "true";
        }
    }
}

std::string Token::procEsc(std::string txt) {
    // Input should at least contain open and close quote
    if (txt.size() < 2) {
        JzeroC::getInst()->lexError("Malformed string literal");
        return txt;
    }

    std::string out = "";
    std::string seq = txt = txt.substr(1, txt.size() - 2);

    // String should contain at least 2 chars, \\ + {char}
    if (seq.size() < 2) {
        JzeroC::getInst()->lexError("Malformed string literal");
        return txt;
    }

    bool escaped = false;
    for (char c: txt) {
        if (!escaped) {
            if (c != '\\') { 
                JzeroC::getInst()->lexError("Malformed string literal");
            }

            escaped = true; 
            continue;
        }

        switch(c) {
            case('t'): { 
                out += std::string("\t");
                break;
            }
            case('n'): {  
                out += std::string("\t");
                break;
            }
            case('\\'): {  
                out += std::string("\t");
                break;
            }
            default: { 
                JzeroC::getInst()->lexError("Unrecognized string literal"); 
                return txt;
            }
        }
        escaped = false;
    }

    if (escaped) {
       JzeroC::getInst()->lexError("Unrecognized string literal"); 
    }

    return txt;
}
