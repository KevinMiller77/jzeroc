#include <iostream>
#include <string>

#include <yacc/yyparse.hpp>

#include <jzeroc/syntax_node.h>
#include <jzeroc/jzeroc.h>
#include <unordered_map>

SyntaxNode::SyntaxNode(std::string s, int r, Token t) {
    this->m_Id = getId();
    this->m_Symbol = s;
    this->m_Rule = r;
    this->m_Token = t;
    this->m_Const = isConst();
}

SyntaxNode::SyntaxNode(std::string s, int r, std::vector<SyntaxNode*> t){
    this->m_Id = getId();
    this->m_Symbol = s;
    this->m_Rule = r;
    this->m_Children = t;
    this->m_Const = isConst();
}

int g_SerialId = 0;
int SyntaxNode::getId() {
    return g_SerialId++;
}

int SyntaxNode::getBaseRule() {
    if (m_Rule < 1000) {
        return m_Rule;
    }

    return m_Rule - (m_Rule % 10);
}

int SyntaxNode::getRuleOffset() {
    if (m_Rule < 1000) {
        return 0;
    }

    return m_Rule % 10;
}

bool SyntaxNode::isConst() {
    switch(m_Rule) {
        case(INTLIT):
        case(DOUBLELIT):
        case(STRINGLIT):
        case(BOOLLIT): {
            return true;
        }
        case(UNARY_EXPR): {
            return m_Children[1]->isConst();
        }
        case(REL_EXPR): {
            return m_Children[0]->isConst() && m_Children[2]->isConst();
        }
        case (COND_AND_EXPR):
        case (COND_OR_EXPR): 
        case (EQ_EXPR):
        case (MUL_EXPR): 
        case (ADD_EXPR): {
            return m_Children[0]->isConst() && m_Children[1]->isConst();
        }
        default: {
            return false;
        }
    }
}


void SyntaxNode::createSymbolTables(SymbolTable* curr) {
    m_SymbolTable = curr;

    // Current symbol is a terminal, no need to proc yet
    if (getBaseRule() < 1000) {
        return;
    }
    
    switch (getBaseRule()) {
        case(SYMBOL_RULES::CLASS_DECL): { 
            SyntaxNode* identifier = m_Children[0];
            if (!identifier || identifier->m_Symbol.empty()) {
                JzeroC::getInst()->logError("Class identifier is empty but shouldn't be!", "SyntaxNode::createSymbolTables");
                break;
            }

            curr = new SymbolTable(identifier->m_Symbol, curr); 
            break;
        }
        case(SYMBOL_RULES::METHOD_DECL): { 
            SyntaxNode* header = m_Children[0];
            if (!header) {
                JzeroC::getInst()->logError("Method header is empty but shouldn't be!", "SyntaxNode::createSymbolTables");
                break;
            }

            SyntaxNode* returnType = header->m_Children[0];
            if (!returnType || returnType->m_Symbol.empty()) {
                JzeroC::getInst()->logError("Method return type is empty but shouldn't be!", "SyntaxNode::createSymbolTables");
                break;
            }

            SyntaxNode* declarator = header->m_Children[1];
            if (!declarator) {
                JzeroC::getInst()->logError("Method declarator is empty but shouldn't be!", "SyntaxNode::createSymbolTables");
                break;
            }

            SyntaxNode* identifier = declarator->m_Children[0];
            if (!identifier || identifier->m_Symbol.empty()) {
                JzeroC::getInst()->logError("Method identifier is empty but shouldn't be!", "SyntaxNode::createSymbolTables");
                break;
            }

            curr = new SymbolTable(identifier->m_Symbol, curr); 
            break; 
        }
    }

    for(SyntaxNode* child: m_Children) {
        if (child == nullptr) {
            continue;
        }

        child->createSymbolTables(curr);
    }
}

void SyntaxNode::populateSymbolTables() {
    switch(getBaseRule()) {
        case(SYMBOL_RULES::CLASS_DECL): {
            m_SymbolTable->insert(
                m_Children[0]->m_Token.m_Text,
                this,
                false,
                m_Children[0]->m_SymbolTable
            );
            break;
        }
        case(SYMBOL_RULES::FIELD_DECL):
        case(SYMBOL_RULES::LOCAL_VAR_DECL): {
            SyntaxNode* type = m_Children[0];
            if (!type || type->m_Token.isEmpty()) {
                JzeroC::getInst()->logError("Type is empty on var decl but shouldn't be!", "SyntaxNode::populateSymbolTables");
                return;
            }

            insertVarDelcs(type->m_Token.m_Text, m_Children[1]);
            break;
        }
        case(SYMBOL_RULES::METHOD_DECL): {
            SyntaxNode* header = m_Children[0]; 
            if (!header) {
                JzeroC::getInst()->logError("Method header is empty but shouldn't be!", "SyntaxNode::populateSymbolTables");
                break;
            }

            SyntaxNode* returnType = header->m_Children[0];
            if (!returnType || returnType->m_Token.isEmpty()) {
                JzeroC::getInst()->logError("Method return type is empty but shouldn't be!", "SyntaxNode::populateSymbolTables");
                break;
            }

            SyntaxNode* declarator = header->m_Children[1];
            if (!declarator) {
                JzeroC::getInst()->logError("Method declarator is empty but shouldn't be!", "SyntaxNode::populateSymbolTables");
                break;
            }

            SyntaxNode* identifier = declarator->m_Children[0];
            if (!identifier || identifier->m_Token.isEmpty()) {
                JzeroC::getInst()->logError("Method identifier is empty but shouldn't be!", "SyntaxNode::populateSymbolTables");
                break;
            }

            m_SymbolTable->insert(
                identifier->m_Token.m_Text, 
                this,
                false,
                header->m_SymbolTable
            );
            break;
        }
        case(SYMBOL_RULES::FORMAL_PARAM): {
            SyntaxNode* type = m_Children[0];
            if (!type || type->m_Token.isEmpty()) {
                JzeroC::getInst()->logError("Type is empty on formal param but shouldn't be!", "SyntaxNode::populateSymbolTables");
                return;
            }

            insertVarDelcs(type->m_Token.m_Text, m_Children[1]);
            break;
        }
    }

    for (SyntaxNode* child: m_Children) {
        if (child == nullptr) {
            continue;
        }

        child->populateSymbolTables();
    }
}

void SyntaxNode::insertVarDelcs(std::string type, SyntaxNode* node, bool array) {
    if (!node) { 
        return; 
    }

    // TODO (Kevin): Use the type string

    if (node->getBaseRule() != SYMBOL_RULES::VAR_DECLS) {
        // This is an array decl, call this function with its child
        if (node->getBaseRule() == SYMBOL_RULES::VAR_DECL) {
            insertVarDelcs(type, node->m_Children[0], true);
            return;
        }
    
        if (node->getBaseRule() == SYMBOL_RULES::ASSIGNMENT) {
            insertVarDelcs(type, node->m_Children[0]);
            return;
        }

        // Not an array and there is only one decl, check the token 
        // exists and can be inserted
        if (node->m_Token.isEmpty()) {
            JzeroC::getInst()->logError("Token is empty but shouldn't be!", "SyntaxNode::insertVarDelcs");
            return;
        }

        m_SymbolTable->insert(
            node->m_Token.m_Text,
            this,
            false
        );
        return;
    }

    insertVarDelcs(type, node->m_Children[0]);
    insertVarDelcs(type, node->m_Children[1]);
}

void SyntaxNode::validateSymbolTables() {
    validateBlocks();
}


void SyntaxNode::validateBlocks() {
    switch(getBaseRule()) {
        case(METHOD_DECL): {
            SyntaxNode* block = m_Children[1];
            
            if (!block) {
                JzeroC::getInst()->logError("Method header is empty but shouldn't be!", "SyntaxNode::validateBodies");
                break;
            }

            block->detectUndeclaredVars();
            break;
        }
        default: {
            for (SyntaxNode* child: m_Children) {
                if (child == nullptr) {
                    continue;
                }

                child->validateBlocks();
            }
        }
    }
}



Symbol* SyntaxNode::detectUndeclaredVars() {
    switch(getBaseRule()) {
        case(IDENTIFIER): {
            Symbol* symbol = m_SymbolTable->lookup(m_Token.m_Text);
            if (!symbol) {
                printUndefinedSymbolError();
                break;
            }

            return symbol;
        }
        case(FIELD_ACCESS):
        case(QUALIFIED_NAME): {
            SyntaxNode* name = m_Children[0];
            if (!name) {
                JzeroC::getInst()->logError("Identifier is empty but shouldn't be!", "SyntaxNode::detectUndeclaredVars");
                break;
            }

            Symbol* scopeSymbol = name->detectUndeclaredVars();
            if (!scopeSymbol) {
                break;
            }

            if (!scopeSymbol->m_Table) {
                printUndefinedSymbolError();
                break;
            }

            Symbol* childScope = scopeSymbol->m_Table->lookup(m_Children[1]->m_Token.m_Text);
            if (!childScope) {
                printUndefinedSymbolError();
                return nullptr;
            }

            return childScope;
        }
        case(METHOD_CALL): {
            SyntaxNode* identifier = m_Children[0];
            if (!identifier) {
                JzeroC::getInst()->logError("Identifier is empty but shouldn't be!", "SyntaxNode::detectUndeclaredVars");
                break;
            }
            
            SyntaxNode* args = m_Children[getRuleOffset() == 0 ? 1 : 2];
            if (!args) {
                JzeroC::getInst()->logError("Args is empty but shouldn't be!", "SyntaxNode::detectUndeclaredVars");
                break;
            }

            args->detectUndeclaredVars();
            identifier->detectUndeclaredVars();
        }
        default: {
            for (SyntaxNode* child: m_Children) {
                if (child == nullptr) {
                    continue;
                }

                child->detectUndeclaredVars();
            }
            break;
        }
    }

    return nullptr;
}

void SyntaxNode::print(int idn) {
    if (idn == 0) {
        std::cout << "\tSyntax Tree" << std::endl;
        std::cout << "----------------------------" << std::endl;
    }
    for (int i = 0; i < idn; i++) std::cout << "  ";

    std::cout << m_Symbol << ":" << m_Rule << " (" << m_Id << ") " << "Const: " << m_Const;
    std::cout << std::endl;

    for (int i = 0; i < m_Children.size(); i++) {
        if (m_Children[i] == nullptr) {
            continue;
        }

        m_Children[i]->print(idn + 1);
    }
}

void SyntaxNode::printSymbols(int idn, bool force) {
    if (idn == 0) {
        std::cout << "\tSymbol Tree" << std::endl;
        std::cout << "----------------------------" << std::endl;
    }

    if (m_Rule == SYMBOL_RULES::CLASS_DECL || force) {
        m_SymbolTable->print(idn);
    }


    for (SyntaxNode* child: m_Children) {
        if (!child) {
            continue;
        }

        child->printSymbols(idn + 1);
    }
}


void SyntaxNode::printUndefinedSymbolError() {
    std::string err = "Undeclared symbol (" + m_Children[1]->m_Token.m_Text + ") at " + m_SymbolTable->getQualifiedParent();
    err += ": Ln " + std::to_string(m_Children[1]->m_Token.m_Line) + ", Col " + std::to_string(m_Children[1]->m_Token.m_Col);
    JzeroC::getInst()->logError(err.c_str());
}