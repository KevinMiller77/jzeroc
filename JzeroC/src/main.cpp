#include <iostream>
#include <fstream>

#include <jzeroc/jzeroc.h>

#include <yacc/yyparse.hpp>

int main(int argc, char** argv) {
    if (argc != 2) {
        std::cerr << "Usage: ./jzeroc infile outfile(opt)\n";
        return 1;
    }

    if (argc == 2) {
        JzeroC::init(argv[1]);
    } else {
        JzeroC::init(argv[1], argv[2]);
    }


    if (!JzeroC::getInst()->isReady()) {
        std::cerr << "Could not initialize scanner! Exiting!\n";
        return 1;
    }

    // We no longer run this directly, the parser will inject scanner calls
    // JzeroC::getInst()->run();
    int parseErrs = yyparse();
    if (parseErrs != 0) {
        std::cerr << parseErrs << " parse errors detected!" << std::endl;
        return 1;
    }

    std::cout << "No parse errors detected!" << std::endl;
    return 0;
}