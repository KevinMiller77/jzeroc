/* A Bison parser, made by GNU Bison 3.8.2.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015, 2018-2021 Free Software Foundation,
   Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* DO NOT RELY ON FEATURES THAT ARE NOT DOCUMENTED in the manual,
   especially those whose name start with YY_ or yy_.  They are
   private implementation details that can be changed or removed.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output, and Bison version.  */
#define YYBISON 30802

/* Bison version string.  */
#define YYBISON_VERSION "3.8.2"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* First part of user prologue.  */
#line 1 "../JzeroC/src/yacc/javagrammar.ypp"

    #include <jzeroc/jzeroc.h>

    #define JZEROC JzeroC::getInst()

    int yylex() { return JZEROC->doLex(); }
    void yyerror(const char* err) { JZEROC->parseError(err); }


#line 81 "../JzeroC/src/yacc/yyparse.cpp"

# ifndef YY_CAST
#  ifdef __cplusplus
#   define YY_CAST(Type, Val) static_cast<Type> (Val)
#   define YY_REINTERPRET_CAST(Type, Val) reinterpret_cast<Type> (Val)
#  else
#   define YY_CAST(Type, Val) ((Type) (Val))
#   define YY_REINTERPRET_CAST(Type, Val) ((Type) (Val))
#  endif
# endif
# ifndef YY_NULLPTR
#  if defined __cplusplus
#   if 201103L <= __cplusplus
#    define YY_NULLPTR nullptr
#   else
#    define YY_NULLPTR 0
#   endif
#  else
#   define YY_NULLPTR ((void*)0)
#  endif
# endif

#include "yyparse.hpp"
/* Symbol kind.  */
enum yysymbol_kind_t
{
  YYSYMBOL_YYEMPTY = -2,
  YYSYMBOL_YYEOF = 0,                      /* "end of file"  */
  YYSYMBOL_YYerror = 1,                    /* error  */
  YYSYMBOL_YYUNDEF = 2,                    /* "invalid token"  */
  YYSYMBOL_BREAK = 3,                      /* BREAK  */
  YYSYMBOL_DOUBLE = 4,                     /* DOUBLE  */
  YYSYMBOL_ELSE = 5,                       /* ELSE  */
  YYSYMBOL_FOR = 6,                        /* FOR  */
  YYSYMBOL_IF = 7,                         /* IF  */
  YYSYMBOL_INT = 8,                        /* INT  */
  YYSYMBOL_RETURN = 9,                     /* RETURN  */
  YYSYMBOL_VOID = 10,                      /* VOID  */
  YYSYMBOL_WHILE = 11,                     /* WHILE  */
  YYSYMBOL_IDENTIFIER = 12,                /* IDENTIFIER  */
  YYSYMBOL_CLASSNAME = 13,                 /* CLASSNAME  */
  YYSYMBOL_CLASS = 14,                     /* CLASS  */
  YYSYMBOL_STRING = 15,                    /* STRING  */
  YYSYMBOL_BOOL = 16,                      /* BOOL  */
  YYSYMBOL_INTLIT = 17,                    /* INTLIT  */
  YYSYMBOL_DOUBLELIT = 18,                 /* DOUBLELIT  */
  YYSYMBOL_STRINGLIT = 19,                 /* STRINGLIT  */
  YYSYMBOL_BOOLLIT = 20,                   /* BOOLLIT  */
  YYSYMBOL_NULLVAL = 21,                   /* NULLVAL  */
  YYSYMBOL_LESSTHANOREQUAL = 22,           /* LESSTHANOREQUAL  */
  YYSYMBOL_GREATERTHANOREQUAL = 23,        /* GREATERTHANOREQUAL  */
  YYSYMBOL_ISEQUALTO = 24,                 /* ISEQUALTO  */
  YYSYMBOL_NOTEQUALTO = 25,                /* NOTEQUALTO  */
  YYSYMBOL_LOGICALAND = 26,                /* LOGICALAND  */
  YYSYMBOL_LOGICALOR = 27,                 /* LOGICALOR  */
  YYSYMBOL_INCREMENT = 28,                 /* INCREMENT  */
  YYSYMBOL_DECREMENT = 29,                 /* DECREMENT  */
  YYSYMBOL_PUBLIC = 30,                    /* PUBLIC  */
  YYSYMBOL_STATIC = 31,                    /* STATIC  */
  YYSYMBOL_YYERRCODE = 32,                 /* YYERRCODE  */
  YYSYMBOL_33_ = 33,                       /* ';'  */
  YYSYMBOL_34_ = 34,                       /* '{'  */
  YYSYMBOL_35_ = 35,                       /* '}'  */
  YYSYMBOL_36_ = 36,                       /* '.'  */
  YYSYMBOL_37_ = 37,                       /* ','  */
  YYSYMBOL_38_ = 38,                       /* '['  */
  YYSYMBOL_39_ = 39,                       /* ']'  */
  YYSYMBOL_40_ = 40,                       /* '('  */
  YYSYMBOL_41_ = 41,                       /* ')'  */
  YYSYMBOL_42_ = 42,                       /* '-'  */
  YYSYMBOL_43_ = 43,                       /* '!'  */
  YYSYMBOL_44_ = 44,                       /* '*'  */
  YYSYMBOL_45_ = 45,                       /* '/'  */
  YYSYMBOL_46_ = 46,                       /* '%'  */
  YYSYMBOL_47_ = 47,                       /* '+'  */
  YYSYMBOL_48_ = 48,                       /* '<'  */
  YYSYMBOL_49_ = 49,                       /* '>'  */
  YYSYMBOL_50_ = 50,                       /* '='  */
  YYSYMBOL_YYACCEPT = 51,                  /* $accept  */
  YYSYMBOL_ClassDecl = 52,                 /* ClassDecl  */
  YYSYMBOL_ClassBody = 53,                 /* ClassBody  */
  YYSYMBOL_ClassBodyDecls = 54,            /* ClassBodyDecls  */
  YYSYMBOL_ClassBodyDecl = 55,             /* ClassBodyDecl  */
  YYSYMBOL_FieldDecl = 56,                 /* FieldDecl  */
  YYSYMBOL_Type = 57,                      /* Type  */
  YYSYMBOL_Name = 58,                      /* Name  */
  YYSYMBOL_QualifiedName = 59,             /* QualifiedName  */
  YYSYMBOL_VarDecls = 60,                  /* VarDecls  */
  YYSYMBOL_VarDecl = 61,                   /* VarDecl  */
  YYSYMBOL_MethodRetVal = 62,              /* MethodRetVal  */
  YYSYMBOL_MethodDecl = 63,                /* MethodDecl  */
  YYSYMBOL_MethodHeader = 64,              /* MethodHeader  */
  YYSYMBOL_MethodDeclarator = 65,          /* MethodDeclarator  */
  YYSYMBOL_ConstructorDecl = 66,           /* ConstructorDecl  */
  YYSYMBOL_FormalParamListOpt = 67,        /* FormalParamListOpt  */
  YYSYMBOL_FormalParamList = 68,           /* FormalParamList  */
  YYSYMBOL_FormalParam = 69,               /* FormalParam  */
  YYSYMBOL_Block = 70,                     /* Block  */
  YYSYMBOL_BlockStmtsOpt = 71,             /* BlockStmtsOpt  */
  YYSYMBOL_BlockStmts = 72,                /* BlockStmts  */
  YYSYMBOL_BlockStmt = 73,                 /* BlockStmt  */
  YYSYMBOL_LocalVarDeclStmt = 74,          /* LocalVarDeclStmt  */
  YYSYMBOL_LocalVarDecl = 75,              /* LocalVarDecl  */
  YYSYMBOL_Stmt = 76,                      /* Stmt  */
  YYSYMBOL_ExprStmt = 77,                  /* ExprStmt  */
  YYSYMBOL_StmtExpr = 78,                  /* StmtExpr  */
  YYSYMBOL_IfThenStmt = 79,                /* IfThenStmt  */
  YYSYMBOL_IfThenElseStmt = 80,            /* IfThenElseStmt  */
  YYSYMBOL_IfThenElseIfStmt = 81,          /* IfThenElseIfStmt  */
  YYSYMBOL_ElseIfSeq = 82,                 /* ElseIfSeq  */
  YYSYMBOL_ElseIfStmt = 83,                /* ElseIfStmt  */
  YYSYMBOL_WhileStmt = 84,                 /* WhileStmt  */
  YYSYMBOL_ForStmt = 85,                   /* ForStmt  */
  YYSYMBOL_ForInit = 86,                   /* ForInit  */
  YYSYMBOL_ExprOpt = 87,                   /* ExprOpt  */
  YYSYMBOL_ForUpdate = 88,                 /* ForUpdate  */
  YYSYMBOL_StmtExprList = 89,              /* StmtExprList  */
  YYSYMBOL_BreakStmt = 90,                 /* BreakStmt  */
  YYSYMBOL_ReturnStmt = 91,                /* ReturnStmt  */
  YYSYMBOL_Primary = 92,                   /* Primary  */
  YYSYMBOL_Literal = 93,                   /* Literal  */
  YYSYMBOL_ArgList = 94,                   /* ArgList  */
  YYSYMBOL_ArgListOpt = 95,                /* ArgListOpt  */
  YYSYMBOL_FieldAccess = 96,               /* FieldAccess  */
  YYSYMBOL_MethodCall = 97,                /* MethodCall  */
  YYSYMBOL_PostFixExpr = 98,               /* PostFixExpr  */
  YYSYMBOL_UnaryExpr = 99,                 /* UnaryExpr  */
  YYSYMBOL_MulExpr = 100,                  /* MulExpr  */
  YYSYMBOL_AddExpr = 101,                  /* AddExpr  */
  YYSYMBOL_RelOp = 102,                    /* RelOp  */
  YYSYMBOL_RelExpr = 103,                  /* RelExpr  */
  YYSYMBOL_EqExpr = 104,                   /* EqExpr  */
  YYSYMBOL_CondAndExpr = 105,              /* CondAndExpr  */
  YYSYMBOL_CondOrExpr = 106,               /* CondOrExpr  */
  YYSYMBOL_Expr = 107,                     /* Expr  */
  YYSYMBOL_Assignment = 108,               /* Assignment  */
  YYSYMBOL_LeftVal = 109,                  /* LeftVal  */
  YYSYMBOL_AssignOp = 110                  /* AssignOp  */
};
typedef enum yysymbol_kind_t yysymbol_kind_t;




#ifdef short
# undef short
#endif

/* On compilers that do not define __PTRDIFF_MAX__ etc., make sure
   <limits.h> and (if available) <stdint.h> are included
   so that the code can choose integer types of a good width.  */

#ifndef __PTRDIFF_MAX__
# include <limits.h> /* INFRINGES ON USER NAME SPACE */
# if defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stdint.h> /* INFRINGES ON USER NAME SPACE */
#  define YY_STDINT_H
# endif
#endif

/* Narrow types that promote to a signed type and that can represent a
   signed or unsigned integer of at least N bits.  In tables they can
   save space and decrease cache pressure.  Promoting to a signed type
   helps avoid bugs in integer arithmetic.  */

#ifdef __INT_LEAST8_MAX__
typedef __INT_LEAST8_TYPE__ yytype_int8;
#elif defined YY_STDINT_H
typedef int_least8_t yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef __INT_LEAST16_MAX__
typedef __INT_LEAST16_TYPE__ yytype_int16;
#elif defined YY_STDINT_H
typedef int_least16_t yytype_int16;
#else
typedef short yytype_int16;
#endif

/* Work around bug in HP-UX 11.23, which defines these macros
   incorrectly for preprocessor constants.  This workaround can likely
   be removed in 2023, as HPE has promised support for HP-UX 11.23
   (aka HP-UX 11i v2) only through the end of 2022; see Table 2 of
   <https://h20195.www2.hpe.com/V2/getpdf.aspx/4AA4-7673ENW.pdf>.  */
#ifdef __hpux
# undef UINT_LEAST8_MAX
# undef UINT_LEAST16_MAX
# define UINT_LEAST8_MAX 255
# define UINT_LEAST16_MAX 65535
#endif

#if defined __UINT_LEAST8_MAX__ && __UINT_LEAST8_MAX__ <= __INT_MAX__
typedef __UINT_LEAST8_TYPE__ yytype_uint8;
#elif (!defined __UINT_LEAST8_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST8_MAX <= INT_MAX)
typedef uint_least8_t yytype_uint8;
#elif !defined __UINT_LEAST8_MAX__ && UCHAR_MAX <= INT_MAX
typedef unsigned char yytype_uint8;
#else
typedef short yytype_uint8;
#endif

#if defined __UINT_LEAST16_MAX__ && __UINT_LEAST16_MAX__ <= __INT_MAX__
typedef __UINT_LEAST16_TYPE__ yytype_uint16;
#elif (!defined __UINT_LEAST16_MAX__ && defined YY_STDINT_H \
       && UINT_LEAST16_MAX <= INT_MAX)
typedef uint_least16_t yytype_uint16;
#elif !defined __UINT_LEAST16_MAX__ && USHRT_MAX <= INT_MAX
typedef unsigned short yytype_uint16;
#else
typedef int yytype_uint16;
#endif

#ifndef YYPTRDIFF_T
# if defined __PTRDIFF_TYPE__ && defined __PTRDIFF_MAX__
#  define YYPTRDIFF_T __PTRDIFF_TYPE__
#  define YYPTRDIFF_MAXIMUM __PTRDIFF_MAX__
# elif defined PTRDIFF_MAX
#  ifndef ptrdiff_t
#   include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  endif
#  define YYPTRDIFF_T ptrdiff_t
#  define YYPTRDIFF_MAXIMUM PTRDIFF_MAX
# else
#  define YYPTRDIFF_T long
#  define YYPTRDIFF_MAXIMUM LONG_MAX
# endif
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif defined __STDC_VERSION__ && 199901 <= __STDC_VERSION__
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned
# endif
#endif

#define YYSIZE_MAXIMUM                                  \
  YY_CAST (YYPTRDIFF_T,                                 \
           (YYPTRDIFF_MAXIMUM < YY_CAST (YYSIZE_T, -1)  \
            ? YYPTRDIFF_MAXIMUM                         \
            : YY_CAST (YYSIZE_T, -1)))

#define YYSIZEOF(X) YY_CAST (YYPTRDIFF_T, sizeof (X))


/* Stored state numbers (used for stacks). */
typedef yytype_uint8 yy_state_t;

/* State numbers in computations.  */
typedef int yy_state_fast_t;

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif


#ifndef YY_ATTRIBUTE_PURE
# if defined __GNUC__ && 2 < __GNUC__ + (96 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_PURE __attribute__ ((__pure__))
# else
#  define YY_ATTRIBUTE_PURE
# endif
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# if defined __GNUC__ && 2 < __GNUC__ + (7 <= __GNUC_MINOR__)
#  define YY_ATTRIBUTE_UNUSED __attribute__ ((__unused__))
# else
#  define YY_ATTRIBUTE_UNUSED
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YY_USE(E) ((void) (E))
#else
# define YY_USE(E) /* empty */
#endif

/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
#if defined __GNUC__ && ! defined __ICC && 406 <= __GNUC__ * 100 + __GNUC_MINOR__
# if __GNUC__ * 100 + __GNUC_MINOR__ < 407
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")
# else
#  define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN                           \
    _Pragma ("GCC diagnostic push")                                     \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")              \
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# endif
# define YY_IGNORE_MAYBE_UNINITIALIZED_END      \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif

#if defined __cplusplus && defined __GNUC__ && ! defined __ICC && 6 <= __GNUC__
# define YY_IGNORE_USELESS_CAST_BEGIN                          \
    _Pragma ("GCC diagnostic push")                            \
    _Pragma ("GCC diagnostic ignored \"-Wuseless-cast\"")
# define YY_IGNORE_USELESS_CAST_END            \
    _Pragma ("GCC diagnostic pop")
#endif
#ifndef YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_BEGIN
# define YY_IGNORE_USELESS_CAST_END
#endif


#define YY_ASSERT(E) ((void) (0 && (E)))

#if !defined yyoverflow

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* !defined yyoverflow */

#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yy_state_t yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (YYSIZEOF (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (YYSIZEOF (yy_state_t) + YYSIZEOF (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYPTRDIFF_T yynewbytes;                                         \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * YYSIZEOF (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / YYSIZEOF (*yyptr);                        \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, YY_CAST (YYSIZE_T, (Count)) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYPTRDIFF_T yyi;                      \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  4
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   299

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  51
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  60
/* YYNRULES -- Number of rules.  */
#define YYNRULES  126
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  202

/* YYMAXUTOK -- Last valid token kind.  */
#define YYMAXUTOK   287


/* YYTRANSLATE(TOKEN-NUM) -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, with out-of-bounds checking.  */
#define YYTRANSLATE(YYX)                                \
  (0 <= (YYX) && (YYX) <= YYMAXUTOK                     \
   ? YY_CAST (yysymbol_kind_t, yytranslate[YYX])        \
   : YYSYMBOL_YYUNDEF)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex.  */
static const yytype_int8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    43,     2,     2,     2,    46,     2,     2,
      40,    41,    44,    47,    37,    42,    36,    45,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,    33,
      48,    50,    49,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    38,     2,    39,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    34,     2,    35,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32
};

#if YYDEBUG
/* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint8 yyrline[] =
{
       0,    27,    27,    33,    34,    37,    38,    42,    42,    42,
      42,    44,    48,    48,    48,    48,    48,    49,    49,    50,
      54,    54,    58,    59,    62,    64,    64,    66,    70,    74,
      78,    82,    82,    83,    83,    86,    90,    94,    94,    95,
      95,    98,    98,   100,   101,   105,   105,   105,   105,   105,
     106,   106,   106,   107,   107,   109,   110,   110,   112,   113,
     114,   115,   117,   117,   120,   124,   128,   131,   131,   131,
     132,   132,   133,   133,   134,   134,   138,   139,   143,   144,
     145,   146,   148,   148,   148,   148,   148,   150,   150,   153,
     153,   154,   158,   161,   165,   165,   166,   167,   168,   170,
     171,   172,   173,   175,   176,   177,   179,   179,   179,   179,
     180,   180,   184,   185,   186,   188,   188,   192,   192,   196,
     196,   198,   202,   202,   203,   203,   203
};
#endif

/** Accessing symbol of state STATE.  */
#define YY_ACCESSING_SYMBOL(State) YY_CAST (yysymbol_kind_t, yystos[State])

#if YYDEBUG || 0
/* The user-facing name of the symbol whose (internal) number is
   YYSYMBOL.  No bounds checking.  */
static const char *yysymbol_name (yysymbol_kind_t yysymbol) YY_ATTRIBUTE_UNUSED;

/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of file\"", "error", "\"invalid token\"", "BREAK", "DOUBLE",
  "ELSE", "FOR", "IF", "INT", "RETURN", "VOID", "WHILE", "IDENTIFIER",
  "CLASSNAME", "CLASS", "STRING", "BOOL", "INTLIT", "DOUBLELIT",
  "STRINGLIT", "BOOLLIT", "NULLVAL", "LESSTHANOREQUAL",
  "GREATERTHANOREQUAL", "ISEQUALTO", "NOTEQUALTO", "LOGICALAND",
  "LOGICALOR", "INCREMENT", "DECREMENT", "PUBLIC", "STATIC", "YYERRCODE",
  "';'", "'{'", "'}'", "'.'", "','", "'['", "']'", "'('", "')'", "'-'",
  "'!'", "'*'", "'/'", "'%'", "'+'", "'<'", "'>'", "'='", "$accept",
  "ClassDecl", "ClassBody", "ClassBodyDecls", "ClassBodyDecl", "FieldDecl",
  "Type", "Name", "QualifiedName", "VarDecls", "VarDecl", "MethodRetVal",
  "MethodDecl", "MethodHeader", "MethodDeclarator", "ConstructorDecl",
  "FormalParamListOpt", "FormalParamList", "FormalParam", "Block",
  "BlockStmtsOpt", "BlockStmts", "BlockStmt", "LocalVarDeclStmt",
  "LocalVarDecl", "Stmt", "ExprStmt", "StmtExpr", "IfThenStmt",
  "IfThenElseStmt", "IfThenElseIfStmt", "ElseIfSeq", "ElseIfStmt",
  "WhileStmt", "ForStmt", "ForInit", "ExprOpt", "ForUpdate",
  "StmtExprList", "BreakStmt", "ReturnStmt", "Primary", "Literal",
  "ArgList", "ArgListOpt", "FieldAccess", "MethodCall", "PostFixExpr",
  "UnaryExpr", "MulExpr", "AddExpr", "RelOp", "RelExpr", "EqExpr",
  "CondAndExpr", "CondOrExpr", "Expr", "Assignment", "LeftVal", "AssignOp", YY_NULLPTR
};

static const char *
yysymbol_name (yysymbol_kind_t yysymbol)
{
  return yytname[yysymbol];
}
#endif

#define YYPACT_NINF (-177)

#define yypact_value_is_default(Yyn) \
  ((Yyn) == YYPACT_NINF)

#define YYTABLE_NINF (-124)

#define yytable_value_is_error(Yyn) \
  0

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
static const yytype_int16 yypact[] =
{
     -23,     6,    39,     2,  -177,    11,    36,    20,  -177,  -177,
       7,  -177,  -177,   -12,  -177,  -177,    99,  -177,  -177,   135,
      34,  -177,  -177,    31,    31,  -177,  -177,   112,   282,  -177,
    -177,   165,  -177,  -177,  -177,  -177,  -177,   245,    14,    35,
      43,    40,  -177,     5,  -177,  -177,    29,    62,   176,    55,
    -177,  -177,   135,    63,    68,  -177,  -177,  -177,    96,   245,
     245,   172,    40,  -177,  -177,   186,   -24,    74,    38,    84,
      86,    76,  -177,   245,  -177,   135,    80,   113,  -177,  -177,
    -177,   245,  -177,    97,    95,   101,   245,   102,  -177,   135,
     172,  -177,   104,   176,  -177,  -177,   111,  -177,  -177,   115,
    -177,  -177,  -177,  -177,  -177,  -177,  -177,   110,  -177,  -177,
      43,  -177,   112,     7,  -177,    14,  -177,  -177,  -177,   245,
     245,   245,   245,   245,  -177,  -177,  -177,  -177,   245,   245,
     245,   245,   245,  -177,   124,   108,  -177,    43,  -177,   123,
    -177,  -177,   235,   245,   131,  -177,   245,   140,  -177,  -177,
    -177,  -177,  -177,  -177,  -177,  -177,   186,   186,   -24,    74,
      74,    38,    84,   245,  -177,   245,  -177,  -177,   153,   152,
     158,  -177,   163,  -177,   164,   245,   259,    31,   208,  -177,
     174,  -177,   213,  -177,   259,     4,   216,  -177,   182,   152,
     173,  -177,  -177,     4,  -177,    31,   245,  -177,  -177,   183,
      31,  -177
};

/* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
   Performed when YYTABLE does not specify something else to do.  Zero
   means the default is an error.  */
static const yytype_int8 yydefact[] =
{
       0,     0,     0,     0,     1,     0,     0,     0,    13,    12,
      17,    15,    14,     0,     4,    10,     0,     5,     7,     0,
      16,    18,     8,     0,     0,     9,     2,    32,     0,     3,
       6,    17,    82,    83,    85,    84,    86,     0,   122,     0,
      20,     0,    78,    80,    81,    24,     0,     0,    38,     0,
      30,    17,     0,     0,    31,    33,    26,    25,     0,     0,
       0,    95,    94,    98,    99,   103,   110,   112,   115,   117,
     119,     0,   120,    90,    11,     0,     0,     0,   125,   126,
     124,     0,    19,     0,     0,     0,    71,     0,    46,     0,
      16,    45,     0,    37,    39,    41,     0,    42,    47,     0,
      50,    51,    52,    53,    54,    48,    49,    57,    56,    27,
      35,    29,     0,     0,    28,    95,    80,    96,    97,     0,
       0,     0,     0,     0,   106,   107,   108,   109,     0,     0,
       0,     0,     0,    79,    89,     0,    87,    21,    23,    91,
     121,    76,    69,     0,     0,    70,     0,    44,    36,    40,
      43,    55,    34,   100,   101,   102,   105,   104,   111,   113,
     114,   116,   118,     0,    92,    90,    68,    74,     0,    67,
       0,    77,     0,    88,     0,    71,     0,     0,     0,    93,
       0,    75,    58,    65,    73,     0,    60,    62,     0,    72,
       0,    59,    64,     0,    63,     0,     0,    61,    66,     0,
       0,    58
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -177,   233,  -177,  -177,   218,  -177,     0,    -6,  -177,   146,
     -26,  -177,  -177,  -177,   178,  -177,  -177,  -177,   125,   -19,
    -177,  -177,   145,  -177,    98,    66,  -177,  -141,  -176,  -177,
    -177,  -177,    59,  -177,  -177,  -177,    71,  -177,    65,  -177,
    -177,   -16,  -177,  -177,    93,   -30,   -33,  -177,   -35,   -28,
     132,  -177,     3,   128,   129,  -177,   -25,   -11,  -177,  -177
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_uint8 yydefgoto[] =
{
       0,    15,     7,    16,    17,    18,    89,   115,    21,    39,
      40,    58,    22,    23,    24,    25,    53,    54,    55,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   186,   187,   103,   104,   168,   144,   188,   169,   105,
     106,    62,    42,   134,   135,    43,    44,    63,    64,    65,
      66,   128,    67,    68,    69,    70,   136,    72,    46,    81
};

/* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule whose
   number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_int16 yytable[] =
{
      20,   167,     3,    41,    49,    50,    19,     1,    45,   192,
      20,   190,    71,    38,     5,   107,    19,   192,   122,    28,
       3,    20,    20,   123,   117,   118,   110,    52,    57,   116,
     116,    61,    41,  -123,  -123,   181,    41,   108,    48,     4,
       8,    45,    90,   167,     9,     6,    38,    27,    10,   137,
      47,    11,    12,    26,    73,  -123,   140,    78,    79,    41,
     107,   145,   129,   130,    45,    48,    13,    61,    74,    38,
      47,    14,    75,    41,    82,    61,    77,    41,    45,    80,
      61,    76,   108,    38,   153,   154,   155,    90,   109,   116,
     116,   116,   116,   116,   156,   157,   124,   125,   116,   116,
     116,   116,   116,     8,   111,   112,    20,     9,   113,   107,
     131,    10,    52,   132,    11,    12,     8,   133,   170,   138,
       9,   172,   126,   127,    51,   139,    41,    11,    12,    13,
     141,   108,   159,   160,    29,   142,    90,    61,   173,   148,
      61,   143,   146,   107,   150,   107,   -81,    31,   151,   164,
     145,   107,    32,    33,    34,    35,    36,    61,   182,    61,
      41,   163,    41,   165,   171,   108,   191,   108,    41,    61,
      38,   199,    38,   108,   197,    37,   198,    75,    38,    83,
       8,   201,    84,    85,     9,    86,   175,    87,    51,   176,
      61,    11,    12,    32,    33,    34,    35,    36,   -22,   177,
    -122,  -122,   -22,   -22,   178,   179,   -22,   184,    47,    88,
      48,    83,    73,   196,    84,    85,    37,    86,   185,    87,
      51,   193,  -122,   195,   200,    32,    33,    34,    35,    36,
     119,   120,   121,     2,    30,   147,   114,   152,   149,     8,
     166,    88,    48,     9,   183,   194,   180,    51,    37,   189,
      11,    12,    32,    33,    34,    35,    36,    51,   174,   161,
     158,   162,    32,    33,    34,    35,    36,     0,     0,     0,
       0,    51,     0,     0,     0,    37,    32,    33,    34,    35,
      36,     0,     0,     0,     0,    37,     8,    59,    60,     0,
       9,     0,    56,     0,    51,     0,     0,    11,    12,    37
};

static const yytype_int16 yycheck[] =
{
       6,   142,    14,    19,    23,    24,     6,    30,    19,   185,
      16,     7,    37,    19,    12,    48,    16,   193,    42,    31,
      14,    27,    28,    47,    59,    60,    52,    27,    28,    59,
      60,    37,    48,    28,    29,   176,    52,    48,    34,     0,
       4,    52,    48,   184,     8,    34,    52,    40,    12,    75,
      36,    15,    16,    33,    40,    50,    81,    28,    29,    75,
      93,    86,    24,    25,    75,    34,    30,    73,    33,    75,
      36,    35,    37,    89,    12,    81,    36,    93,    89,    50,
      86,    38,    93,    89,   119,   120,   121,    93,    33,   119,
     120,   121,   122,   123,   122,   123,    22,    23,   128,   129,
     130,   131,   132,     4,    41,    37,   112,     8,    12,   142,
      26,    12,   112,    27,    15,    16,     4,    41,   143,    39,
       8,   146,    48,    49,    12,    12,   142,    15,    16,    30,
      33,   142,   129,   130,    35,    40,   142,   143,   163,    35,
     146,    40,    40,   176,    33,   178,    36,    12,    33,    41,
     175,   184,    17,    18,    19,    20,    21,   163,   177,   165,
     176,    37,   178,    40,    33,   176,   185,   178,   184,   175,
     176,   196,   178,   184,   193,    40,   195,    37,   184,     3,
       4,   200,     6,     7,     8,     9,    33,    11,    12,    37,
     196,    15,    16,    17,    18,    19,    20,    21,    33,    41,
      28,    29,    37,    38,    41,    41,    41,    33,    36,    33,
      34,     3,    40,    40,     6,     7,    40,     9,     5,    11,
      12,     5,    50,    41,    41,    17,    18,    19,    20,    21,
      44,    45,    46,     0,    16,    89,    58,   112,    93,     4,
     142,    33,    34,     8,   178,   186,   175,    12,    40,   184,
      15,    16,    17,    18,    19,    20,    21,    12,   165,   131,
     128,   132,    17,    18,    19,    20,    21,    -1,    -1,    -1,
      -1,    12,    -1,    -1,    -1,    40,    17,    18,    19,    20,
      21,    -1,    -1,    -1,    -1,    40,     4,    42,    43,    -1,
       8,    -1,    10,    -1,    12,    -1,    -1,    15,    16,    40
};

/* YYSTOS[STATE-NUM] -- The symbol kind of the accessing symbol of
   state STATE-NUM.  */
static const yytype_int8 yystos[] =
{
       0,    30,    52,    14,     0,    12,    34,    53,     4,     8,
      12,    15,    16,    30,    35,    52,    54,    55,    56,    57,
      58,    59,    63,    64,    65,    66,    33,    40,    31,    35,
      55,    12,    17,    18,    19,    20,    21,    40,    58,    60,
      61,    92,    93,    96,    97,   108,   109,    36,    34,    70,
      70,    12,    57,    67,    68,    69,    10,    57,    62,    42,
      43,    58,    92,    98,    99,   100,   101,   103,   104,   105,
     106,   107,   108,    40,    33,    37,    38,    36,    28,    29,
      50,   110,    12,     3,     6,     7,     9,    11,    33,    57,
      58,    70,    71,    72,    73,    74,    75,    76,    77,    78,
      79,    80,    81,    84,    85,    90,    91,    97,   108,    33,
      61,    41,    37,    12,    65,    58,    96,    99,    99,    44,
      45,    46,    42,    47,    22,    23,    48,    49,   102,    24,
      25,    26,    27,    41,    94,    95,   107,    61,    39,    12,
     107,    33,    40,    40,    87,   107,    40,    60,    35,    73,
      33,    33,    69,    99,    99,    99,   100,   100,   101,   103,
     103,   104,   105,    37,    41,    40,    75,    78,    86,    89,
     107,    33,   107,   107,    95,    33,    37,    41,    41,    41,
      87,    78,    70,    76,    33,     5,    82,    83,    88,    89,
       7,    70,    79,     5,    83,    41,    40,    70,    70,   107,
      41,    70
};

/* YYR1[RULE-NUM] -- Symbol kind of the left-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr1[] =
{
       0,    51,    52,    53,    53,    54,    54,    55,    55,    55,
      55,    56,    57,    57,    57,    57,    57,    58,    58,    59,
      60,    60,    61,    61,    61,    62,    62,    63,    64,    65,
      66,    67,    67,    68,    68,    69,    70,    71,    71,    72,
      72,    73,    73,    74,    75,    76,    76,    76,    76,    76,
      76,    76,    76,    76,    76,    77,    78,    78,    79,    80,
      81,    81,    82,    82,    83,    84,    85,    86,    86,    86,
      87,    87,    88,    88,    89,    89,    90,    91,    92,    92,
      92,    92,    93,    93,    93,    93,    93,    94,    94,    95,
      95,    96,    97,    97,    98,    98,    99,    99,    99,   100,
     100,   100,   100,   101,   101,   101,   102,   102,   102,   102,
     103,   103,   104,   104,   104,   105,   105,   106,   106,   107,
     107,   108,   109,   109,   110,   110,   110
};

/* YYR2[RULE-NUM] -- Number of symbols on the right-hand side of rule RULE-NUM.  */
static const yytype_int8 yyr2[] =
{
       0,     2,     5,     3,     2,     1,     2,     1,     1,     1,
       1,     3,     1,     1,     1,     1,     1,     1,     1,     3,
       1,     3,     1,     3,     1,     1,     1,     3,     4,     4,
       2,     1,     0,     1,     3,     2,     3,     1,     0,     1,
       2,     1,     1,     2,     2,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     2,     1,     1,     5,     7,
       6,     8,     1,     2,     2,     5,     9,     1,     1,     0,
       1,     0,     1,     0,     1,     3,     2,     3,     1,     3,
       1,     1,     1,     1,     1,     1,     1,     1,     3,     1,
       0,     3,     4,     6,     1,     1,     2,     2,     1,     1,
       3,     3,     3,     1,     3,     3,     1,     1,     1,     1,
       1,     3,     1,     3,     3,     1,     3,     1,     3,     1,
       1,     3,     1,     1,     1,     1,     1
};


enum { YYENOMEM = -2 };

#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYNOMEM         goto yyexhaustedlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                    \
  do                                                              \
    if (yychar == YYEMPTY)                                        \
      {                                                           \
        yychar = (Token);                                         \
        yylval = (Value);                                         \
        YYPOPSTACK (yylen);                                       \
        yystate = *yyssp;                                         \
        goto yybackup;                                            \
      }                                                           \
    else                                                          \
      {                                                           \
        yyerror (YY_("syntax error: cannot back up")); \
        YYERROR;                                                  \
      }                                                           \
  while (0)

/* Backward compatibility with an undocumented macro.
   Use YYerror or YYUNDEF. */
#define YYERRCODE YYUNDEF


/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)




# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Kind, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*-----------------------------------.
| Print this symbol's value on YYO.  |
`-----------------------------------*/

static void
yy_symbol_value_print (FILE *yyo,
                       yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  FILE *yyoutput = yyo;
  YY_USE (yyoutput);
  if (!yyvaluep)
    return;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/*---------------------------.
| Print this symbol on YYO.  |
`---------------------------*/

static void
yy_symbol_print (FILE *yyo,
                 yysymbol_kind_t yykind, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyo, "%s %s (",
             yykind < YYNTOKENS ? "token" : "nterm", yysymbol_name (yykind));

  yy_symbol_value_print (yyo, yykind, yyvaluep);
  YYFPRINTF (yyo, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yy_state_t *yybottom, yy_state_t *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yy_state_t *yyssp, YYSTYPE *yyvsp,
                 int yyrule)
{
  int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %d):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       YY_ACCESSING_SYMBOL (+yyssp[yyi + 1 - yynrhs]),
                       &yyvsp[(yyi + 1) - (yynrhs)]);
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args) ((void) 0)
# define YY_SYMBOL_PRINT(Title, Kind, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif






/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg,
            yysymbol_kind_t yykind, YYSTYPE *yyvaluep)
{
  YY_USE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yykind, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YY_USE (yykind);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}


/* Lookahead token kind.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;




/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    yy_state_fast_t yystate = 0;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus = 0;

    /* Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* Their size.  */
    YYPTRDIFF_T yystacksize = YYINITDEPTH;

    /* The state stack: array, bottom, top.  */
    yy_state_t yyssa[YYINITDEPTH];
    yy_state_t *yyss = yyssa;
    yy_state_t *yyssp = yyss;

    /* The semantic value stack: array, bottom, top.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs = yyvsa;
    YYSTYPE *yyvsp = yyvs;

  int yyn;
  /* The return value of yyparse.  */
  int yyresult;
  /* Lookahead symbol kind.  */
  yysymbol_kind_t yytoken = YYSYMBOL_YYEMPTY;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;



#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY; /* Cause a token to be read.  */

  goto yysetstate;


/*------------------------------------------------------------.
| yynewstate -- push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;


/*--------------------------------------------------------------------.
| yysetstate -- set current state (the top of the stack) to yystate.  |
`--------------------------------------------------------------------*/
yysetstate:
  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
  YY_ASSERT (0 <= yystate && yystate < YYNSTATES);
  YY_IGNORE_USELESS_CAST_BEGIN
  *yyssp = YY_CAST (yy_state_t, yystate);
  YY_IGNORE_USELESS_CAST_END
  YY_STACK_PRINT (yyss, yyssp);

  if (yyss + yystacksize - 1 <= yyssp)
#if !defined yyoverflow && !defined YYSTACK_RELOCATE
    YYNOMEM;
#else
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYPTRDIFF_T yysize = yyssp - yyss + 1;

# if defined yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        yy_state_t *yyss1 = yyss;
        YYSTYPE *yyvs1 = yyvs;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * YYSIZEOF (*yyssp),
                    &yyvs1, yysize * YYSIZEOF (*yyvsp),
                    &yystacksize);
        yyss = yyss1;
        yyvs = yyvs1;
      }
# else /* defined YYSTACK_RELOCATE */
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        YYNOMEM;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yy_state_t *yyss1 = yyss;
        union yyalloc *yyptr =
          YY_CAST (union yyalloc *,
                   YYSTACK_ALLOC (YY_CAST (YYSIZE_T, YYSTACK_BYTES (yystacksize))));
        if (! yyptr)
          YYNOMEM;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YY_IGNORE_USELESS_CAST_BEGIN
      YYDPRINTF ((stderr, "Stack size increased to %ld\n",
                  YY_CAST (long, yystacksize)));
      YY_IGNORE_USELESS_CAST_END

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }
#endif /* !defined yyoverflow && !defined YYSTACK_RELOCATE */


  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;


/*-----------.
| yybackup.  |
`-----------*/
yybackup:
  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either empty, or end-of-input, or a valid lookahead.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token\n"));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = YYEOF;
      yytoken = YYSYMBOL_YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else if (yychar == YYerror)
    {
      /* The scanner already issued an error message, process directly
         to error recovery.  But do not keep the error token as
         lookahead, it is too special and may lead us to an endless
         loop in error recovery. */
      yychar = YYUNDEF;
      yytoken = YYSYMBOL_YYerror;
      goto yyerrlab1;
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  /* Discard the shifted token.  */
  yychar = YYEMPTY;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
  case 2: /* ClassDecl: PUBLIC CLASS IDENTIFIER ClassBody ';'  */
#line 27 "../JzeroC/src/yacc/javagrammar.ypp"
                                                          {
                        yyval = new SyntaxNode("ClassDecl", 1000, { yyvsp[-2], yyvsp[-1] });
                        JZEROC->semantic(yyval);
                        // $$->print();
                    }
#line 1353 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 3: /* ClassBody: '{' ClassBodyDecls '}'  */
#line 33 "../JzeroC/src/yacc/javagrammar.ypp"
                                           { yyval = new SyntaxNode("ClassBody", 1010, {yyvsp[-1]}); }
#line 1359 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 4: /* ClassBody: '{' '}'  */
#line 34 "../JzeroC/src/yacc/javagrammar.ypp"
                                           { yyval = new SyntaxNode("ClassBody", 1011); }
#line 1365 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 6: /* ClassBodyDecls: ClassBodyDecls ClassBodyDecl  */
#line 38 "../JzeroC/src/yacc/javagrammar.ypp"
                                                   {
                        yyval = new SyntaxNode("ClassBodyDecls", 1020, {yyvsp[-1], yyvsp[0]});
                    }
#line 1373 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 11: /* FieldDecl: Type VarDecls ';'  */
#line 44 "../JzeroC/src/yacc/javagrammar.ypp"
                                      {
                        yyval = new SyntaxNode("FieldDecl", 1030, { yyvsp[-2], yyvsp[-1] });
                    }
#line 1381 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 19: /* QualifiedName: Name '.' IDENTIFIER  */
#line 50 "../JzeroC/src/yacc/javagrammar.ypp"
                                        {
                        yyval = new SyntaxNode("QualifiedName", 1040, {yyvsp[-2], yyvsp[0]});
                    }
#line 1389 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 21: /* VarDecls: VarDecls ',' VarDecl  */
#line 54 "../JzeroC/src/yacc/javagrammar.ypp"
                                                   {
                        yyval = new SyntaxNode("VarDecls", 1050, {yyvsp[-2], yyvsp[0]});
                    }
#line 1397 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 23: /* VarDecl: VarDecl '[' ']'  */
#line 59 "../JzeroC/src/yacc/javagrammar.ypp"
                                      {
                        yyval = new SyntaxNode("VarDecl", 1060, {yyvsp[-2]});
                    }
#line 1405 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 27: /* MethodDecl: MethodHeader Block ';'  */
#line 66 "../JzeroC/src/yacc/javagrammar.ypp"
                                           {
                        yyval = new SyntaxNode("MethodDecl", 1380, {yyvsp[-2], yyvsp[-1]});
                    }
#line 1413 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 28: /* MethodHeader: PUBLIC STATIC MethodRetVal MethodDeclarator  */
#line 70 "../JzeroC/src/yacc/javagrammar.ypp"
                                                                {
                        yyval = new SyntaxNode("MethodHeader", 1070, {yyvsp[-1], yyvsp[0]});
                    }
#line 1421 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 29: /* MethodDeclarator: IDENTIFIER '(' FormalParamListOpt ')'  */
#line 74 "../JzeroC/src/yacc/javagrammar.ypp"
                                                          {
                        yyval = new SyntaxNode("MethodDeclarator", 1080, {yyvsp[-3], yyvsp[-1]});
                    }
#line 1429 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 30: /* ConstructorDecl: MethodDeclarator Block  */
#line 78 "../JzeroC/src/yacc/javagrammar.ypp"
                                           {
                        yyval = new SyntaxNode("ConstructorDecl", 1110, {yyvsp[-1], yyvsp[0]});
                    }
#line 1437 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 34: /* FormalParamList: FormalParamList ',' FormalParam  */
#line 83 "../JzeroC/src/yacc/javagrammar.ypp"
                                                                  {
                        yyval = new SyntaxNode("FormalParamList", 1090, {yyvsp[-2], yyvsp[0]});
                    }
#line 1445 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 35: /* FormalParam: Type VarDecl  */
#line 86 "../JzeroC/src/yacc/javagrammar.ypp"
                                 {
                        yyval = new SyntaxNode("FormalParam", 1100, {yyvsp[-1], yyvsp[0]});
                    }
#line 1453 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 36: /* Block: '{' BlockStmtsOpt '}'  */
#line 90 "../JzeroC/src/yacc/javagrammar.ypp"
                                          {
                        yyval = new SyntaxNode("Block", 1120, {yyvsp[-1]});
                    }
#line 1461 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 40: /* BlockStmts: BlockStmts BlockStmt  */
#line 95 "../JzeroC/src/yacc/javagrammar.ypp"
                                                     {
                        yyval = new SyntaxNode("BlockStmts", 1130, {yyvsp[-1], yyvsp[0]});
                    }
#line 1469 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 44: /* LocalVarDecl: Type VarDecls  */
#line 101 "../JzeroC/src/yacc/javagrammar.ypp"
                                  {
                        yyval = new SyntaxNode("LocalVarDecl", 1140, {yyvsp[-1], yyvsp[0]});
                    }
#line 1477 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 58: /* IfThenStmt: IF '(' Expr ')' Block  */
#line 112 "../JzeroC/src/yacc/javagrammar.ypp"
                                          { yyval = new SyntaxNode("IfThenStmt", 1150, {yyvsp[-2], yyvsp[0]}); }
#line 1483 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 59: /* IfThenElseStmt: IF '(' Expr ')' Block ELSE Block  */
#line 113 "../JzeroC/src/yacc/javagrammar.ypp"
                                                     { yyval = new SyntaxNode("IfThenElseStmt", 1160, {yyvsp[-4], yyvsp[-2], yyvsp[0]}); }
#line 1489 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 60: /* IfThenElseIfStmt: IF '(' Expr ')' Block ElseIfSeq  */
#line 114 "../JzeroC/src/yacc/javagrammar.ypp"
                                                    { yyval = new SyntaxNode("IfThenElseIfStmt", 1170, {yyvsp[-3], yyvsp[-1], yyvsp[0]}); }
#line 1495 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 61: /* IfThenElseIfStmt: IF '(' Expr ')' Block ElseIfSeq ELSE Block  */
#line 115 "../JzeroC/src/yacc/javagrammar.ypp"
                                                               { yyval = new SyntaxNode("IfThenElseIfStmt", 1171, {yyvsp[-5], yyvsp[-3], yyvsp[-2], yyvsp[0]}); }
#line 1501 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 63: /* ElseIfSeq: ElseIfSeq ElseIfStmt  */
#line 117 "../JzeroC/src/yacc/javagrammar.ypp"
                                                      {
                        yyval = new SyntaxNode("ElseIfSeq", 1180, {yyvsp[-1], yyvsp[0]});
                    }
#line 1509 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 64: /* ElseIfStmt: ELSE IfThenStmt  */
#line 120 "../JzeroC/src/yacc/javagrammar.ypp"
                                    {
                        yyval = new SyntaxNode("ElseIfStmt", 1190, {yyvsp[0]});
                    }
#line 1517 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 65: /* WhileStmt: WHILE '(' Expr ')' Stmt  */
#line 124 "../JzeroC/src/yacc/javagrammar.ypp"
                                            {
                        yyval = new SyntaxNode("WhileStmt", 1210, {yyvsp[-2], yyvsp[0]});
                    }
#line 1525 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 66: /* ForStmt: FOR '(' ForInit ';' ExprOpt ';' ForUpdate ')' Block  */
#line 128 "../JzeroC/src/yacc/javagrammar.ypp"
                                                                        {
                        yyval = new SyntaxNode("ForStmt", 1220, {yyvsp[-6], yyvsp[-4], yyvsp[-2], yyvsp[0]});
                    }
#line 1533 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 75: /* StmtExprList: StmtExprList ',' StmtExpr  */
#line 134 "../JzeroC/src/yacc/javagrammar.ypp"
                                                         {
                        yyval = new SyntaxNode("StmtExprList", 1230, {yyvsp[-2], yyvsp[0]});
                    }
#line 1541 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 77: /* ReturnStmt: RETURN ExprOpt ';'  */
#line 139 "../JzeroC/src/yacc/javagrammar.ypp"
                                       {
                        yyval = new SyntaxNode("ReturnStmt", 1250, {yyvsp[-1]});
                    }
#line 1549 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 79: /* Primary: '(' Expr ')'  */
#line 144 "../JzeroC/src/yacc/javagrammar.ypp"
                                   { yyval = yyvsp[-1]; }
#line 1555 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 88: /* ArgList: ArgList ',' Expr  */
#line 150 "../JzeroC/src/yacc/javagrammar.ypp"
                                            {
                        yyval = new SyntaxNode("ArgList", 1270, {yyvsp[-2], yyvsp[0]});
                    }
#line 1563 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 91: /* FieldAccess: Primary '.' IDENTIFIER  */
#line 154 "../JzeroC/src/yacc/javagrammar.ypp"
                                           {
                        yyval = new SyntaxNode("FieldAccess", 1280, {yyvsp[-2], yyvsp[0]});
                    }
#line 1571 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 92: /* MethodCall: Name '(' ArgListOpt ')'  */
#line 158 "../JzeroC/src/yacc/javagrammar.ypp"
                                            {
                        yyval = new SyntaxNode("MethodCall", 1290, {yyvsp[-3], yyvsp[-1]});
                    }
#line 1579 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 93: /* MethodCall: Primary '.' IDENTIFIER '(' ArgListOpt ')'  */
#line 161 "../JzeroC/src/yacc/javagrammar.ypp"
                                                                {
                        yyval = new SyntaxNode("MethodCall", 1291, {yyvsp[-5], yyvsp[-3], yyvsp[-1]});
                    }
#line 1587 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 96: /* UnaryExpr: '-' UnaryExpr  */
#line 166 "../JzeroC/src/yacc/javagrammar.ypp"
                                    { yyval = new SyntaxNode("UnaryExpr", 1300, {yyvsp[-1], yyvsp[0]}); }
#line 1593 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 97: /* UnaryExpr: '!' UnaryExpr  */
#line 167 "../JzeroC/src/yacc/javagrammar.ypp"
                                    { yyval = new SyntaxNode("UnaryExpr", 1301, {yyvsp[-1], yyvsp[0]}); }
#line 1599 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 100: /* MulExpr: MulExpr '*' UnaryExpr  */
#line 171 "../JzeroC/src/yacc/javagrammar.ypp"
                                            { yyval = new SyntaxNode("MulExpr", 1310, {yyvsp[-2], yyvsp[0]}); }
#line 1605 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 101: /* MulExpr: MulExpr '/' UnaryExpr  */
#line 172 "../JzeroC/src/yacc/javagrammar.ypp"
                                            { yyval = new SyntaxNode("MulExpr", 1311, {yyvsp[-2], yyvsp[0]}); }
#line 1611 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 102: /* MulExpr: MulExpr '%' UnaryExpr  */
#line 173 "../JzeroC/src/yacc/javagrammar.ypp"
                                            { yyval = new SyntaxNode("MulExpr", 1312, {yyvsp[-2], yyvsp[0]}); }
#line 1617 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 104: /* AddExpr: AddExpr '+' MulExpr  */
#line 176 "../JzeroC/src/yacc/javagrammar.ypp"
                                          { yyval = new SyntaxNode("AddExpr", 1320, {yyvsp[-2], yyvsp[0]}); }
#line 1623 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 105: /* AddExpr: AddExpr '-' MulExpr  */
#line 177 "../JzeroC/src/yacc/javagrammar.ypp"
                                          { yyval = new SyntaxNode("AddExpr", 1321, {yyvsp[-2], yyvsp[0]}); }
#line 1629 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 111: /* RelExpr: RelExpr RelOp AddExpr  */
#line 180 "../JzeroC/src/yacc/javagrammar.ypp"
                                                    {
                        yyval = new SyntaxNode("RelExpr", 1330, {yyvsp[-2], yyvsp[-1], yyvsp[0]});
                    }
#line 1637 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 113: /* EqExpr: EqExpr ISEQUALTO RelExpr  */
#line 185 "../JzeroC/src/yacc/javagrammar.ypp"
                                               { yyval = new SyntaxNode("EqExpr", 1340, {yyvsp[-2], yyvsp[0]}); }
#line 1643 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 114: /* EqExpr: EqExpr NOTEQUALTO RelExpr  */
#line 186 "../JzeroC/src/yacc/javagrammar.ypp"
                                                { yyval = new SyntaxNode("EqExpr", 1341, {yyvsp[-2], yyvsp[0]}); }
#line 1649 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 116: /* CondAndExpr: CondAndExpr LOGICALAND EqExpr  */
#line 188 "../JzeroC/src/yacc/javagrammar.ypp"
                                                           {
                        yyval = new SyntaxNode("CondAndExpr", 1350, {yyvsp[-2], yyvsp[0]});
                    }
#line 1657 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 118: /* CondOrExpr: CondOrExpr LOGICALOR CondAndExpr  */
#line 192 "../JzeroC/src/yacc/javagrammar.ypp"
                                                                   {
                        yyval = new SyntaxNode("CondOrExpr", 1360, {yyvsp[-2], yyvsp[0]});
                    }
#line 1665 "../JzeroC/src/yacc/yyparse.cpp"
    break;

  case 121: /* Assignment: LeftVal AssignOp Expr  */
#line 198 "../JzeroC/src/yacc/javagrammar.ypp"
                                          {
                        yyval = new SyntaxNode("Assignment", 1370, {yyvsp[-2], yyvsp[-1], yyvsp[0]});
                    }
#line 1673 "../JzeroC/src/yacc/yyparse.cpp"
    break;


#line 1677 "../JzeroC/src/yacc/yyparse.cpp"

      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", YY_CAST (yysymbol_kind_t, yyr1[yyn]), &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */
  {
    const int yylhs = yyr1[yyn] - YYNTOKENS;
    const int yyi = yypgoto[yylhs] + *yyssp;
    yystate = (0 <= yyi && yyi <= YYLAST && yycheck[yyi] == *yyssp
               ? yytable[yyi]
               : yydefgoto[yylhs]);
  }

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYSYMBOL_YYEMPTY : YYTRANSLATE (yychar);
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
      yyerror (YY_("syntax error"));
    }

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:
  /* Pacify compilers when the user code never invokes YYERROR and the
     label yyerrorlab therefore never appears in user code.  */
  if (0)
    YYERROR;
  ++yynerrs;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  /* Pop stack until we find a state that shifts the error token.  */
  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYSYMBOL_YYerror;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYSYMBOL_YYerror)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  YY_ACCESSING_SYMBOL (yystate), yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", YY_ACCESSING_SYMBOL (yyn), yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturnlab;


/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturnlab;


/*-----------------------------------------------------------.
| yyexhaustedlab -- YYNOMEM (memory exhaustion) comes here.  |
`-----------------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturnlab;


/*----------------------------------------------------------.
| yyreturnlab -- parsing is finished, clean up and return.  |
`----------------------------------------------------------*/
yyreturnlab:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  YY_ACCESSING_SYMBOL (+*yyssp), yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif

  return yyresult;
}

